<?php

use Illuminate\Database\Seeder;

class EmployeesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('employees')->insert([
            [
                'employee' => 4,
                "manager" => 3
            ],
            [
                'employee' => 5,
                "manager" => 3
            ],
            [
                'employee' => 6,
                "manager" => 8
            ],
            [
                'employee' => 7,
                "manager" => 9
            ]
        ]);

    }
}
