<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            [
            'id'=> 4,
            'name' => 'Noam11',
            'email' => "noam11@gmail.com",
            'password' => Hash::make('12345678'),
            'created_at' => date('Y-m-d G:i:s'),
            'role' => 'employee'
            ],
            [
            'id'=>5,
            'name' => 'Yoni12',
            'email' => "Yoni12@gmail.com",
            'password' => Hash::make('12345678'),
            'created_at' => date('Y-m-d G:i:s'),
            'role' => 'employee'
            ],
            [
                'id'=> 6,
                'name' => 'Noa',
                'email' => "noa@gmail.com",
                'password' => Hash::make('12345678'),
                'created_at' => date('Y-m-d G:i:s'),
                'role' => 'employee'
                ],
                [
                'id'=>7,
                'name' => 'Yonat',
                'email' => "Yonat@gmail.com",
                'password' => Hash::make('12345678'),
                'created_at' => date('Y-m-d G:i:s'),
                'role' => 'employee'
                ]
        ]);
    }
}
