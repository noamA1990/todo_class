@extends('layouts.app')
@section('content')

<h1>Edit your tesk</h1>

<form method='post' action="{{action('TodoController@update',$todos->id)}}">
    @csrf
    @method('PATCH')
    <div class = "form-group">
        <label for="title">Todo Update:</label>
        <input type="text" class = "form-control" name = "title" value ="{{$todos->title}}">
    </div>

    <div class = "form-group">
        <input type="submit" class = "form-control" name = "submit" value = "Done">
    </div>
</form>

<form method = 'post' action="{{action('TodoController@destroy',$todos->id)}}">
@csrf
@method('DELETE')
<div class = "form-group">
    <input type ="submit" class = "form-control" name="submit" value ="Delete Todo">
</div>
</form>

@endsection